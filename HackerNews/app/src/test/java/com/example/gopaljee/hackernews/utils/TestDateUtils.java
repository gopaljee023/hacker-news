package com.example.gopaljee.hackernews.utils;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.junit.Assert.*;

/**
 * Created by gopaljee on 02/07/17.
 */

public class TestDateUtils {

    @Test
    public void formatNewsApiDate_inputMilisecondsPassedFrom0millisecondFrom01011972_outputShouldbeUnixstartTime()
    {
        String expected = "01/01/1970";
        String actual = DateUtils.formatNewsApiDate("0");

        assertEquals(expected,actual);
    }

    @Test
    public void formatNewsApiDate_inputMiliseconds1YearsMillisecond_outputShouldbeOneYearAfterUnixStartTime()
    {
        String expected = "01/01/1971";
        long OneYearMillisecond = 1*365*24*60*60*1000;
        String actual = DateUtils.formatNewsApiDate(String.valueOf(OneYearMillisecond));
        System.out.println(actual);
        assertEquals(expected,actual);
    }

    @Test
    public void formatNewsApiDate_inputMiliseconds1YearsMillisecond_outputShouldbeOneYearAfterUnixStartTime2()
    {
        String expected = "01/01/1971";
        long OneYearMillisecond =2;
        String actual = DateUtils.formatNewsApiDate(String.valueOf(OneYearMillisecond));
        System.out.println(actual);
        assertEquals(expected,actual);
    }



}

