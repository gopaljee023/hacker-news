package com.example.gopaljee.hackernews.utils;

import org.junit.Test;
import static org.junit.Assert.*;


/**
 * Created by gopaljee on 02/07/17.
 */

public class TestCommentUtils {
    @Test
    public void countTotalCommentMessage_zeroInput_outputAsDiscssString()
    {
        String expected ="discuss";
        String receivedResult = CommentUtils.countTotalCommentMessage(0);
        assertEquals(expected,receivedResult);
    }

    @Test
    public void countTotalCommentMessage_inputAsOne_outputAs1comment()
    {
        String expected ="1 comment";
        String receivedResult = CommentUtils.countTotalCommentMessage(1);
        assertEquals(expected,receivedResult);
    }

    @Test
    public void countTotalCommentMessage_inputAsMoreThanOne_outputAsMoreThancomments()
    {
        String expected ="2 comments";
        String receivedResult = CommentUtils.countTotalCommentMessage(2);
        assertEquals(expected,receivedResult);
    }

    @Test
    public void countTotalPointsMessage_zeroInput_outputAsEmptyString()
    {
        String expected ="";
        String receivedResult = CommentUtils.countTotalPointsMessage(0);
        assertEquals(expected,receivedResult);
    }

    @Test
    public void countTotalPointsMessage_inputAsOne_outputAs1comment()
    {
        String expected ="1 point";
        String receivedResult = CommentUtils.countTotalPointsMessage(1);
        assertEquals(expected,receivedResult);
    }

    @Test
    public void countTotalPointsMessage_inputAsMoreThanOne_outputAsMoreThancomments()
    {
        String expected ="100 points";
        String receivedResult = CommentUtils.countTotalPointsMessage(100);
        assertEquals(expected,receivedResult);
    }
}
