package com.example.gopaljee.hackernews.UI;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.example.gopaljee.hackernews.NewsStore;
import com.example.gopaljee.hackernews.R;
import com.example.gopaljee.hackernews.model.TopStoryDTO;
import com.example.gopaljee.hackernews.service.NewsAPI;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private final static  String TAG = MainActivity.class.getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Call<List<Integer>>  getTopStoriesResponseCall = NewsAPI.getTopStoriesApi().getTopStories();
        getTopStoriesResponseCall.enqueue(new Callback<List<Integer> >() {
            @Override
            public void onResponse(Call< List<Integer> > call, Response< List<Integer> > response) {
                List<Integer> topStoriesIDs = response.body();
                for(Integer id:topStoriesIDs)
                {
                    UpdateTopStories(id);
                }

            }

            @Override
            public void onFailure(Call< List<Integer> > call, Throwable t) {
                Log.d("TAG","failed");
            }
        });

    }


    void UpdateTopStories(Integer id)
    {
        Call<TopStoryDTO> topStoryDTOCall = NewsAPI.getTopStoriesApi().getStory(id);
        topStoryDTOCall.enqueue(new Callback<TopStoryDTO>() {
            @Override
            public void onResponse(Call<TopStoryDTO> call, Response<TopStoryDTO> response) {
             NewsStore.Add(response.body());
                    /*1.get recyclerview objct
                                                   2.setLayoutManager
                                                    3.setadapter
                                                    */
                RecyclerView recyclerView = (RecyclerView) findViewById(R.id.news_list_recycler);

                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(MainActivity.this);
                recyclerView.setLayoutManager(layoutManager);

                HackerNewsAdapter homeNewsAdapter = new HackerNewsAdapter(NewsStore.get_newsArticles());
                recyclerView.setAdapter(homeNewsAdapter);
            }

            @Override
            public void onFailure(Call<TopStoryDTO> call, Throwable t) {
              Log.d(TAG,"Stories failed to get");
            }
        });
    }
}
