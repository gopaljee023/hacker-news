package com.example.gopaljee.hackernews.UI;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.gopaljee.hackernews.R;
import com.example.gopaljee.hackernews.model.TopStoryDTO;
import com.example.gopaljee.hackernews.utils.CommentUtils;
import com.example.gopaljee.hackernews.utils.DateUtils;
import com.example.gopaljee.hackernews.utils.UserMessageUtils;

import java.util.List;

/**
 * Created by gopaljee on 01/07/17.
 */

public class HackerNewsAdapter extends  RecyclerView.Adapter<HackerNewsAdapter.NewsHolder>
{
    private List<TopStoryDTO> _dataSet;
    public HackerNewsAdapter(List<TopStoryDTO> articleList)
    {
        _dataSet = articleList;
    }

    @Override
    public NewsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.news_item,parent,false); //false
        return new NewsHolder(view);
    }

    @Override
    public void onBindViewHolder(NewsHolder holder, final int position) {
        holder.get_textTitle().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //go to webview
                NewsDetailsActivity.launch(v.getContext(),position);
            }
        });
        TopStoryDTO article = _dataSet.get(position);

        holder.get_textTitle().setText(article.getTitle());
        holder.get_textPoints().setText(CommentUtils.countTotalPointsMessage(article.getScore()));
        holder.get_textByUser().setText(UserMessageUtils.userMessage(article.getBy()));
        int commentCount = article.getKids() == null?0:article.getKids().size();
        holder.get_textComments().setText(CommentUtils.countTotalCommentMessage(commentCount));

       // holder.get_textCounter().setText(String.valueOf(position+1));
        holder.get_textTime().setText(DateUtils.formatNewsApiDate(String.valueOf(article.getTime())));

    }


    @Override
    public int getItemCount() {
        return _dataSet.size();
    }

    public static class NewsHolder extends RecyclerView.ViewHolder {

        private TextView _textTitle;
        private TextView _textCounter;
        private TextView _textTime;
        private TextView _textComments;
        private TextView _textByUser;
        private TextView _textPoints;

        public TextView get_textTitle() {
            return _textTitle;
        }

        public TextView get_textCounter() {
            return _textCounter;
        }

        public TextView get_textTime() {
            return _textTime;
        }

        public TextView get_textComments() {
            return _textComments;
        }

        public TextView get_textByUser() {
            return _textByUser;
        }

        public TextView get_textPoints() {
            return _textPoints;
        }

        public NewsHolder(View itemView) {
            super(itemView);
           // _textCounter = (TextView)itemView.findViewById(R.id.card_news_counter);
            _textTitle = (TextView)itemView.findViewById(R.id.card_news_title);
            _textTime = (TextView)itemView.findViewById(R.id.card_news_item_time);
            _textComments = (TextView)itemView.findViewById(R.id.card_news_item_kids_count);
            _textByUser = (TextView)itemView.findViewById(R.id.card_news_item_by_user);

            _textPoints =  (TextView)itemView.findViewById(R.id.card_news_item_score);

        }


    }
}