package com.example.gopaljee.hackernews.utils;

/**
 * Created by gopaljee on 01/07/17.
 */

public class UserMessageUtils {

    public static String userMessage(String userName)
    {
        String message ="";
        if(userName !=null && !userName.isEmpty())
        {
            message = "by "+userName.trim();
        }
        return  message;
    }
}
