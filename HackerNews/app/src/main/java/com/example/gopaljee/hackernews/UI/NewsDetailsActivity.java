package com.example.gopaljee.hackernews.UI;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.gopaljee.hackernews.NewsStore;
import com.example.gopaljee.hackernews.R;
import com.example.gopaljee.hackernews.model.TopStoryDTO;


public class NewsDetailsActivity extends AppCompatActivity {

    public static String KEY_INDEX = "keyindex";
    private WebView _webView;
    private ProgressBar _progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_details);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        _webView = (WebView)findViewById(R.id.activity_news_details_webview);
        _progressBar = (ProgressBar)findViewById(R.id.activity_news_details_progressbar);
        int index = getIntent().getIntExtra(KEY_INDEX,-1);
        if(index!=-1) {
            updateNewsDetails(index);
        }
        else
        {
            Toast.makeText(NewsDetailsActivity.this,"Sorry incorrect index passed", Toast.LENGTH_SHORT).show();
        }


    }

    public void updateNewsDetails(int index)
    {
        //enable java script to load web page properly
        _webView.getSettings().setJavaScriptEnabled(true);

        _webView.setWebViewClient(new WebViewClient(){
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                _progressBar.setProgress(View.VISIBLE);// to show progress bar
                super.onPageStarted(view,url,favicon);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                _progressBar.setProgress(View.GONE); //to stop progress bar
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
                _progressBar.setProgress(View.GONE); //to stop progress bar
                Toast.makeText(NewsDetailsActivity.this,"Error in loading the page",Toast.LENGTH_SHORT).show();
            }
        });

        TopStoryDTO article = NewsStore.get_newsArticles().get(index);
        _webView.loadUrl(article.getUrl());

        //set title of actionbar
        getSupportActionBar().setTitle(NewsStore.get_newsArticles().get(index).getTitle());
    }


    public static void launch(Context context, int index)
    {
        Intent intent = new Intent(context,NewsDetailsActivity.class);
        intent.putExtra(KEY_INDEX,index);
        context.startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
