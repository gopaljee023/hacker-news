package com.example.gopaljee.hackernews.service;

import com.example.gopaljee.hackernews.model.TopStoryDTO;

import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by gopaljee on 01/07/17.
 */

public class NewsAPI {

    private static final String BASEURL="https://hacker-news.firebaseio.com/v0/";

    private static TopStoriesService _topStoriesService;

    public static TopStoriesService getTopStoriesApi()
    {
        if(_topStoriesService==null)
        {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BASEURL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            TopStoriesService service = retrofit.create(TopStoriesService.class);
            _topStoriesService = service;
        }
        return _topStoriesService;
    }

    public interface TopStoriesService {
        @GET("topstories.json")
        Call<List<Integer>> getTopStories();

       // https://hacker-news.firebaseio.com/v0/item/:id.json

        @GET("item/{id}.json")
        Call<TopStoryDTO> getStory(@Path("id") int id);
    }


}
